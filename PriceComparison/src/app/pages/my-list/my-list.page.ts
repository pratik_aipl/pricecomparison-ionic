import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';
import { ModalController, AlertController } from '@ionic/angular';
import { StoreListComponent } from '../store-list/store-list.component';

@Component({
  selector: 'app-my-list',
  templateUrl: './my-list.page.html',
  styleUrls: ['./my-list.page.scss'],
})
export class MyListPage implements OnInit {

  cat: any;
  from= '0';
  catid= '0';
  productList: any = [];
  headerStore: any = [];
  sumLowest = 0;
  sumHeigst = 0;
  saving = 0;
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder, private modalController: ModalController,
    public commonService: CommonService, public alertController: AlertController,
    public tools: Tools) {
    this.cat = this.activatedRoute.snapshot.paramMap.get('catName');
    this.catid = this.activatedRoute.snapshot.paramMap.get('catid');
    this.from = this.activatedRoute.snapshot.paramMap.get('from');

    console.log(this.cat)
    console.log(this.from)
  }
  backPage() {
    if (this.from == '0') {
      this.router.navigateByUrl('/');      
    }else if (this.from == '1') {
      this.router.navigateByUrl('/category-list');
    }else if (this.from == '2') {
      this.router.navigateByUrl('/dashboard/product-list/'+this.cat+'/'+this.catid);
    }else{
      this.router.navigateByUrl('/products-details/'+this.commonService.getProduct().Productid+'/0'+'/'+this.catid); 
    }
  }
  inc(item, action, index) {    if ( parseInt(item.Quantity) != 1) {
      if (action == '-') {
        this.productList[index].Quantity = parseInt(item.Quantity)- 1;
      }
    }
    if (action == '+') {
      this.productList[index].Quantity = parseInt(item.Quantity) + 1;
    }
    this.calculateData();
    this.calculateLowData();
  }
  async productDetails(item) {
    this.commonService.setProduct(item);
    // this.router.navigateByUrl('/dashboard/products-details/'+item.Productname+'/'+item.Productid+'/1');
    // this.router.navigateByUrl('/dashboard/products-details/'+item.Productname+'/'+item.Productid+'/1/0');
    this.router.navigateByUrl('/products-details/'+item.Productid+'/1'+'/'+this.catid);

  }

  async brandList() {
    const modal = await this.modalController.create({
      component: StoreListComponent,
      cssClass: 'modal',
      componentProps: { value: 'head' }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        this.getMyStores();
      });
  }

  ngOnInit() {
    this.getMyStores();
  }
  getMyStores() {
    this.tools.openLoader();
    this.commonService.myStore().then(result => {
     // console.log(JSON.parse(result.data));
      this.headerStore = JSON.parse(result.data).data.userstore;
      this.getMyList();
    }, (error) => {
      this.tools.closeLoader();
      console.log(error.error);
      this.tools.openAlert(error.status, '',JSON.parse(error.error).Message);
    });
  }

  getMyList() {
    //  this.tools.openLoader();
    this.commonService.myProduct().then(result => {
      this.tools.closeLoader();
      console.log(JSON.parse(result.data) );
      if (JSON.parse(result.data).Success) {
        this.productList = JSON.parse(result.data).data.Product;
       }       else{
        this.productList =[];
       }

       this.calculateLowData();   
       this.calculateData();  
    }, (error) => {
      this.tools.closeLoader();
      console.log(JSON.parse(error.error).Message );
      this.tools.openAlert(error.status, '',JSON.parse(error.error).Message);
    });
  }

  calculateLowData() {
    this.sumLowest = 0;
    this.sumHeigst = 0;
    for (let i = 0; i < this.productList.length; i++) {
      const productStoreNew = [];
      const productStore = this.productList[i].Store;
      for (let j = 0; j < productStore.length; j++) {
        const pStore = productStore[j];
        for (let k = 0; k < this.headerStore.length; k++) {
          const Store = this.headerStore[k];
          if (Store.StoreID == pStore.storeid) {
            productStoreNew.push(pStore);
          }
        }
      }
      this.productList[i].isSelected = true;
      this.productList[i].Store = productStoreNew;
      var low = Math.min.apply(Math, productStoreNew.map(function (item) { return item.currentprice }));
      for (let a = 0; a < productStoreNew.length; a++) {
        const element = productStoreNew[a];
        if (parseFloat(element.currentprice) == parseFloat(low)) {
          if (parseFloat(low) == 999999.99) {
            this.productList[i].isStore = false;
          } else {
            this.productList[i].isStore = true;
          }
          this.productList[i].lowimg = element.StoreLogo;
          break;
        }
      }
      this.productList[i].low = low;
      this.productList[i].high = high;
      if (parseFloat(low) == 999999.99) {
        this.sumLowest += 0;
      } else {
        this.sumLowest += parseFloat(low) * parseFloat(this.productList[i].Quantity);
      }

      const hiStoreList = [];
      for (let g = 0; g < this.productList[i].Store.length; g++) {
        const element = this.productList[i].Store[g];
        if (parseFloat(element.currentprice) != 999999.99) {
          hiStoreList.push(element);
        }
      }
      var high = Math.max.apply(Math, hiStoreList.map(function (item) { return item.currentprice }));
      if (parseFloat(high) == 999999.99) {
        this.sumHeigst += 0;
      } else {
        this.sumHeigst += parseFloat(high) * parseFloat(this.productList[i].Quantity);
      }
    }
    this.saving = this.sumHeigst-this.sumLowest;

    console.log(this.saving);

  }
  // convertValue(value){
  //   return value.substr(0, value.indexOf('.') + 3)
  // }
  convertVal(calVal){
   // console.log('calValue ',calVal);
    if (calVal != undefined) {
      return  parseFloat(calVal).toFixed(2);      
    }else{
      return  0.00;
    }

  }
  calculateData() {
    for (let k = 0; k < this.headerStore.length; k++) {
      let sumData = 0;
      for (let i = 0; i < this.productList.length; i++) {
        const product = this.productList[i];
        if(product.Store[k].currentprice === '999999.99'){
          sumData += 0;
        }else{
          // sumData += parseFloat(product.Store[k].currentprice);
          sumData += (parseFloat(product.Store[k].currentprice)*(parseInt(product.Quantity)));
        }
      }     
      this.headerStore[k].sumData =  sumData;
    }
  }
  async removeProduct(item, i) {
    const alert = await this.alertController.create({
      header: 'Remove Product',
      message: 'Are you sure you want remove product?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            alert.dismiss();
            // console.log('Cancel clicked');
          }
        },
        {
          text: 'Remove',
          handler: () => {
            alert.dismiss();
            this.tools.openLoader();
            this.commonService.removeProduct(item.Productid).then(result => {
              this.tools.closeLoader();
              // this.productList.splice(i, 1);
              this.tools.openAlert('', '', JSON.parse(result.data).DMessage);
              this.getMyList();
              this.cartQty();

            }, (error) => {
              this.tools.openAlert(error.status, '',JSON.parse(error.error).Message);
            });
          }
        }
      ], backdropDismiss: false
    });
    return await alert.present();
  }

  cartQty(){
    this.commonService.cartQty().then(result => {
       console.log( JSON.parse(result.data));
      if (JSON.parse(result.data).Success) {
        this.commonService.setQty(JSON.parse(result.data).data.Count);
      }
    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }
}
