import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SharedModule } from 'src/app/shared/shared.module';
import { MyListPage } from './my-list.page';

const routes: Routes = [
  {
    path: '',
    component: MyListPage
  }
];

@NgModule({
  imports: [
  SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyListPage]
})
export class MyListPageModule {}
