import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
      this.loginForm = this.formBuilder.group({
        emailid: ['', [Validators.required,Validators.email]],
        pwd: ['', Validators.required],
       
      });
     }
    //  emailid: ['rita.bhagat@gmail.com', [Validators.required,Validators.email]],
    //  pwd: ['rita', Validators.required],

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    console.log(form);
    this.tools.openLoader();
    this.commonService.login(form).then(result => {
      console.log(result);
      if (JSON.parse(result.data).Success != false) {
        this.commonService.setUserData(JSON.parse(result.data).data.user[0]);
        this.callGetToekn(form);
      }else{
        this.tools.closeLoader();
        this.tools.openAlert('','',JSON.parse(result.data).DMessage);
      }
      
    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }

  callGetToekn(form){
    this.commonService.toekn(form).then(result => {
      this.tools.closeLoader();
     //console.log(result.data.token_type+' '+result.data.access_token);
      this.commonService.setToken (JSON.parse(result.data).token_type+' '+JSON.parse(result.data).access_token);
      this.router.navigateByUrl('/dashboard');
    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }

  goToRegister(){
    this.router.navigateByUrl('/register');
  }
  goToForgotPassword(){
    this.router.navigateByUrl('/forgot-password');
  }
  goToDashboard(){
    this.router.navigateByUrl('/dashboard');
  }
}
