import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, IonSlides } from '@ionic/angular';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.scss'],
})
export class StoreListComponent implements OnInit {
  type: any;
  btnName: any;
  storeList: any = [];
  constructor(public navParams: NavParams, public tools: Tools,
    public commonServices: CommonService, public modalCtrl: ModalController) {
    this.type = this.navParams.get('value');
    console.log(' ==> ' + this.type)
    if (this.type == 'menu') {
      this.getMyStores();
      this.btnName = 'Update My Prefered Store'
    } else {
      this.getUserStores();
      this.btnName = 'Save My Prefered Store'
    }
  }
  ngOnInit() { }


  dismissModal() {
    // environment.isBeaconModalOpen = false;
   //   this.modalCtrl.dismiss('');
  }

  cancel(){
    this.modalCtrl.dismiss('');
  }
  
  close() {

    const storeId:any = [];
    for (let i = 0; i < this.storeList.length; i++) {
      if (this.storeList[i].IsSelected == "1") {
        storeId.push(this.storeList[i].StoreID);
      }
    }
    if (storeId.length == 0) {
      this.tools.openAlert('','Select Store', 'Please select at least one store.');
    } else {
      this.commonServices.saveStore(storeId).then(result => {
        console.log(result);
        this.modalCtrl.dismiss('change');
        if (this.type == 'menu') {
          this.getMyStores();
        }
      }, (error) => {
        this.tools.closeLoader();
        console.log(error);
        if (error.status == '401') {
          this.modalCtrl.dismiss('');
        }
        this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
      });
    }
  }


  changeChacked(event, i) {
    this.storeList[i].IsSelected = event.detail.checked ? "1" : "0"; //result.data.userstore;
  }
  getUserStores() {
    this.commonServices.userStore().then(result => {
      this.storeList = JSON.parse(result.data).data.userstore;
    }, (error) => {
      this.tools.closeLoader();
      console.log(error);
      if (error.status == '401') {
        this.modalCtrl.dismiss('');
      }
      this.tools.openAlert(error.status, '',JSON.parse(error.error).Message);
    });
  }
  getMyStores() {
    this.commonServices.myStore().then(result => {
      console.log('My Store ==> ', this.storeList);
      this.storeList = JSON.parse(result.data).data.userstore;
    }, (error) => {
      this.tools.closeLoader();
      console.log(error);
      if (error.status == '401') {
        this.modalCtrl.dismiss('');
      }
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }


}
