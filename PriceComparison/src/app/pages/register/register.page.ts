import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  
  registerForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
      this.registerForm= this.formBuilder.group({
        fname: ['sam', Validators.required],
        lname: ['dsd', Validators.required],
        emailid: ['sa@gmail.com',[Validators.required, Validators.email.bind(this)]],
        mphone: ['1245124231'],
        pwd: ['123456', Validators.required],
        cpassword: ['123456', Validators.required],
        device_token: ['fsdfsf'],
      });
     }

  ngOnInit() {
  }
  onSubmit(form: NgForm){
    console.log(form);
    this.commonService.register(form).then(result => {
      console.log('Result '+JSON.parse(result.data));
      this.commonService.setUserData(JSON.parse(result.data).data.user[0]);
      // this.router.navigateByUrl('/dashboard');
      this.callGetToekn(form);
    },(error) => {
      this.tools.closeLoader();
      console.log('Error '+error.error);
      console.log('Error '+JSON.parse(error.error).DMessage);
      var msg;
      if(error.status==500){
        msg=JSON.parse(error.error).DMessage
      }else{
        msg=JSON.parse(error.error).Message;
      }
      this.tools.openAlert(error.status,'',msg);
    });
  }
  callGetToekn(form){
    this.commonService.toekn(form).then(result => {
      this.tools.closeLoader();
     //console.log(result.token_type+' '+result.access_token);
      this.commonService.setToken (JSON.parse(result.data).token_type+' '+JSON.parse(result.data).access_token);
      this.router.navigateByUrl('/dashboard');
    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }

  goLogin(){
    console.log('login')
    this.router.navigateByUrl('/');
  }
  goToDashboard(){
    this.router.navigateByUrl('/dashboard');
  }
}
