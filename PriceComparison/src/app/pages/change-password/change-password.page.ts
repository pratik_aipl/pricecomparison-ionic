import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';

@Component({
  selector: 'app-login',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  loginForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
      this.loginForm = this.formBuilder.group({
        UserID: [this.commonService.getUserId()],
        OldPassword: ['', Validators.required],
        NewPassword: ['', Validators.required],
        confirmpassword: ['', Validators.required],
      });

     }
     

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    if(this.loginForm.get('NewPassword').value != this.loginForm.get('confirmpassword').value){
      this.tools.openAlert('','','New Password And Confirm Password Dont Match.');
    }else if(this.loginForm.get('NewPassword').value == this.loginForm.get('OldPassword').value){
      this.tools.openAlert('','','Old Password And New Password Are Similar.');
    }else{
      this.tools.openLoader();
      this.commonService.changepassword(form).then(result => {
      
        if (JSON.parse(result.data).Success != false) {
          this.loginForm.reset();
        }
        console.log(result);
        this.tools.closeLoader();
        this.tools.openAlert('','',JSON.parse(result.data).UMessage);    
      },(error) => {
        this.tools.closeLoader();
        console.log(error);
        this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
      });
    }
  }

  
  backPage(){
    this.router.navigateByUrl('/');
  }
}
