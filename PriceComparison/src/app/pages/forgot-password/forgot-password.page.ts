import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  loginForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required,Validators.email]],
      });
     }
     

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    this.tools.openLoader();
    this.commonService.forgotPassword(form).then(result => {
      this.tools.closeLoader();
      this.tools.openAlert('',JSON.parse(result.data).UMessage,JSON.parse(result.data).DMessage);      
    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });

  }

  goToLogin(){
    this.router.navigateByUrl('/');
  }

}
