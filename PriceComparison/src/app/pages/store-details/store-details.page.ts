import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-store-details',
  templateUrl: './store-details.page.html',
  styleUrls: ['./store-details.page.scss'],
})
export class StoreDetailsPage implements OnInit {

  store: any;
  stid: any;
  platform:any;
  resultData:any;
  storedetail: any = [];
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    private callNumber: CallNumber,
    
    public tools: Tools) {
    this.store = this.activatedRoute.snapshot.paramMap.get('store');
    this.stid = this.activatedRoute.snapshot.paramMap.get('stid');

    this.getStoreDetails();
  }
  backPage() {
    this.router.navigateByUrl('/');
  }
  ngOnInit() {

  }

  getStoreDetails() {
    this.tools.openLoader();
    this.commonService.getStoreDetailsById(this.stid).then(result => {
      console.log(result);
      this.tools.closeLoader();
      this.resultData=JSON.parse(result.data);
      this.storedetail = this.resultData.data.storedetail;
    }, (error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }
  callTo(number) {
    console.log(number);
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }
  gotToAddress(item) {
    let destination = item.StoreLat + ',' + item.StoreLong;
console.log(destination);
console.log(localStorage.getItem('device'));
      if (localStorage.getItem('device')=='ios') {
        window.open('maps://?q=' + destination, '_system');
      } else {
        let label = encodeURI(item.StoreDetailName);
        window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
      }
  }
}
