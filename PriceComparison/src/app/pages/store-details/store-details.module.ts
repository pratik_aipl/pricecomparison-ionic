import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreDetailsPage } from './store-details.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: StoreDetailsPage
  }
];

@NgModule({
  imports: [
  SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StoreDetailsPage]
})
export class StoreDetailsPageModule {}
