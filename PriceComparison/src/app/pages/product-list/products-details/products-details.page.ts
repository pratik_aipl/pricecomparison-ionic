import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder} from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';
import { IonSlides, Events } from '@ionic/angular';

@Component({
  selector: 'app-products-details',
  templateUrl: './products-details.page.html',
  styleUrls: ['./products-details.page.scss'],
})
export class ProductsDetailsPage implements OnInit {

  typePage: any;
  prodId: any;
  prod: any;
  catType: any;
  prodName: any;
  platform:any;
  resultData:any;
  storedetail: any = [];
  slideOptions = {
    initialSlide: 1,
    speed: 400,
  };
  @ViewChild('slider', { static: true }) slider: IonSlides;
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,public events: Events,
    public commonService: CommonService,
    public tools: Tools) {
      this.prod =this.commonService.getProduct();
      this.typePage = this.activatedRoute.snapshot.paramMap.get('type')=='0'?true:false;
      this.prodName = 'Product Details';//this.activatedRoute.snapshot.paramMap.get('prodName');
      this.prodId = this.activatedRoute.snapshot.paramMap.get('prodId');
      this.catType = this.activatedRoute.snapshot.paramMap.get('catType');
  }

  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  }
  backPage() {
    if(this.typePage){
      if(this.catType =='0'){
        this.router.navigateByUrl('/dashboard/product-list/All/0');
      }else{
        // this.router.navigateByUrl('/dashboard/product-list/'+cat.CategoryName+'/'+cat.CategoryID);
        this.router.navigateByUrl('/dashboard/product-list/'+this.prod.categoryname+'/'+this.prod.categoryid);
      }
    }else{
      console.log(this.prod.categoryname);
      console.log(this.prod.categoryid);
      this.router.navigateByUrl('/my-list/3/'+this.prod.categoryname+'/'+this.prod.categoryid);
    }
  }
  ngOnInit() {
    
  }
  inc(item,action){
    if(item.Quantity != 1){
      if(action == '-'){
        this.prod.Quantity = parseFloat(item.Quantity)-1;
      }  
    }
    if(action == '+'){
      this.prod.Quantity= parseFloat(item.Quantity)+1;
    }
    this.events.publish('qtyChange', item,this.prod.Quantity); 
  }
  myList(){
    this.router.navigateByUrl('/my-list/3/'+this.prod.categoryname+'/'+this.prod.categoryid);
  }
  
  next() {
    this.slider.slideNext();
  }

  prev() {
    this.slider.slidePrev();
  }
  
  addProduct(){

    const prodIds:any=[];
    prodIds.push(this.prod.Productid);
    if(prodIds.length != 0){
      this.tools.openLoader();
      this.commonService.addProductToStore(prodIds,this.prod.Quantity).then(result => {
        this.tools.closeLoader();
        this.tools.openAlert('', '', JSON.parse(result.data).DMessage);
        this.cartQty();
            }, (error) => {
        console.log(error);
        this.tools.openAlert(error.status, '',JSON.parse(error.error).Message);
      });
    }else{      
      this.tools.openAlert('','Select Product', 'Please Select At Least One Product.');
    }
  }

  ionViewWillEnter(){
    this.cartQty();
  }

  cartQty(){
// this.tools.openLoader();
    this.commonService.cartQty().then(result => {
//       console.log( JSON.parse(result.data));       
      if (JSON.parse(result.data).Success) {
        this.tools.closeLoader();
        this.commonService.setQty(JSON.parse(result.data).data.Count);       
      }
    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }
}
