import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SharedModule } from 'src/app/shared/shared.module';
import { ProductDescriptionPage } from './products-description.page';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: ProductDescriptionPage
  }
];

@NgModule({
  imports: [
  SharedModule,FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductDescriptionPage]
})
export class ProductDescriptionPageModule {}
