import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductsDetailsPage } from './products-details.page';


const routes: Routes = [
  {
    path: "tabs",
    component: ProductsDetailsPage,
    children: [
      {
        path: "",
        redirectTo: "tabs/(PriceComparison:PriceComparison)",
        pathMatch: "full"
      },
      {
        path: "PriceComparison",
        children: [
          {
            path: "",
            loadChildren: "../products-details/price-comparison/price-comparison.module#PriceComparisonPageModule"
          }
        ]
      },
      {
        path: "ProductDescription",
        children: [
          {
            path: "",
            loadChildren: "../products-details/product-description/product-description.module#ProductDescriptionPageModule"
          }
        ]
      },
      {
        path: "NutritionsFacts",
        children: [
          {
            path: "",
            loadChildren:
              "../products-details/nutritions-facts/nutritions-facts.module#NutritionsFactsPageModule"
          }
        ]
      },
      // {
      //   path: "Comments",
      //   children: [
      //     {
      //       path: "",
      //       loadChildren:
      //         "../products-details/comments/commnets.module#CommentsPageModule"
      //     }
      //   ]
      // }
    ]
  },
  {
    path: "",
    redirectTo: "tabs/PriceComparison",
    pathMatch: "full"
  }
];

// const routes: Routes = [
//   {
//     path: '',
//     component: MyprofilePage,
//     children:[
//       { path: 'mydetails', loadChildren: '../myprofile/mydetails/mydetails.module#MydetailsPageModule' },
//       { path: 'myrides', loadChildren: '../myprofile/myrides/myrides.module#MyridesPageModule' },
//   ]
//   },{
//     path:'',
//     redirectTo:'../myprofile/mydetails',
//     pathMatch:'full'
//   }
// ];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductsDetailsPage]
})
export class ProductsDetailsPageModule {}
