import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-nutritions-facts',
  templateUrl: './nutritions-facts.page.html',
  styleUrls: ['./nutritions-facts.page.scss'],
})
export class NutritionsFactsPage implements OnInit {

  cmt: any;
  prod: any;
  commentList:any=[];
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
      this.prod =this.commonService.getProduct();
 
      console.log
        
   // this.getStoreDetails();
  }
 
  getProductsComments() {
  //  this.tools.openLoader();
    this.commonService.getProductCommentsById(this.prod.Productid).then(result => {
      console.log(result);
      this.tools.closeLoader();
      this.commentList = JSON.parse(result.data).data.comment;
    }, (error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',error.error.Message);
    });
      }
  ngOnInit() {
    this.getProductsComments();
  }
  
  addComment(){
    this.tools.openLoader();
    this.commonService.createComment(this.prod.Productid, this.cmt).then(result => {
      console.log(result);
      this.cmt = '';
      this.getProductsComments();
    }, (error) => {
      this.tools.closeLoader();
      console.log(error.error);
      this.tools.openAlert(error.status,'',error.error.Message);
    });
  
  }
 
}
