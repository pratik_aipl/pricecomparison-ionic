import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SharedModule } from 'src/app/shared/shared.module';
import { NutritionsFactsPage } from './nutritions-facts.page';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: NutritionsFactsPage
  }
];

@NgModule({
  imports: [
  SharedModule,FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NutritionsFactsPage]
})
export class NutritionsFactsPageModule {}
