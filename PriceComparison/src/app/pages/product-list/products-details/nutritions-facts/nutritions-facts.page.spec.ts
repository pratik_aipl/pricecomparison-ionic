import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionsFactsPage } from './nutritions-facts.page';

describe('ProductListPage', () => {
  let component: NutritionsFactsPage;
  let fixture: ComponentFixture<NutritionsFactsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NutritionsFactsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionsFactsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
