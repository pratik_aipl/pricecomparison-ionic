import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';
import { IonSlides, Events } from '@ionic/angular';

@Component({
  selector: 'app-price-comparison',
  templateUrl: './price-comparison.page.html',
  styleUrls: ['./price-comparison.page.scss'],
})
export class PriceComparisonPage implements OnInit {

  cmt: any;
  prod: any;
  highStoreImage='';
  lowStoreImage='';
  highStoreValue:any;
  lowStoreValue:any;;
  varienseStore:any;

  commentList:any=[];
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,public events: Events,
    public commonService: CommonService,
    public tools: Tools) {
      this.prod =this.commonService.getProduct();
      
      events.subscribe('qtyChange', (item, Quantity) => {
        this.prod = item;
        this.setData(Quantity);
      });
      this.setData(this.prod.Quantity);
  }
  convertVal(calVal){
    return  parseFloat(calVal).toFixed(2);
  }
  setData(Quantity) {
    var lowValue = Math.min.apply(Math, this.prod.Store.map(function (item) {return item.currentprice;}));

     for (let i = 0; i < this.prod.Store.length; i++) {
      const element = this.prod.Store[i];
      if(lowValue == element.currentprice ){
        this.lowStoreImage = element.StoreLogo;
      }
    }

    this.lowStoreValue = lowValue == 999999.99 ? '-' : '£' + this.convertVal(lowValue * Quantity);
    const storeList = [];
    for (let i = 0; i < this.prod.Store.length; i++) {
      const element = this.prod.Store[i];
      if (parseFloat(element.currentprice) != 999999.99) {
        storeList.push(element);
      }
    }
    var higValue = storeList.length != 0 ? Math.max.apply(Math, storeList.map(function (item) {return item.currentprice;})) : 999999.99;

    for (let i = 0; i < storeList.length; i++) {
      const element = storeList[i];
      if(higValue == element.currentprice ){
        this.highStoreImage = element.StoreLogo;
      }
    }
    console.log('Low Value Logo --> ',this.lowStoreImage);
    console.log('High Value Logo --> ',this.highStoreImage);

    this.highStoreValue = higValue == 999999.99 ? '-' : '£' + this.convertVal(higValue * Quantity);
    var variense = (higValue - lowValue);
    this.varienseStore = variense == 999999.99 ? "-" : '£' + this.convertVal(variense * Quantity);
    }
    

  convertValue(value){
    return value.substr(0, value.indexOf('.') + 3)
  }

  getProductsComments() {
    this.commonService.getProductCommentsById(this.prod.Productid).then(result => {
      console.log(result);
      this.tools.closeLoader();
      this.commentList = JSON.parse(result.data).data.comment;
    }, (error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',error.error.Message);
    });
    
  }
  ngOnInit() {
    this.getProductsComments();
  }
  addComment(){
    this.tools.openLoader();
    this.commonService.createComment(this.prod.Productid, this.cmt).then(result => {
      console.log(result);
      this.cmt = '';
      this.getProductsComments();
    }, (error) => {
      this.tools.closeLoader();
      console.log(error.error);
      this.tools.openAlert(error.status,'',error.error.Message);
    });
  
  }
}
