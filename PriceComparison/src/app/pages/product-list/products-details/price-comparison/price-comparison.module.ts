import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SharedModule } from 'src/app/shared/shared.module';
import { PriceComparisonPage } from './price-comparison.page';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: PriceComparisonPage
  }
];

@NgModule({
  imports: [
  SharedModule,FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PriceComparisonPage]
})
export class PriceComparisonPageModule {}
