import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceComparisonPage } from './price-comparison.page';

describe('ProductListPage', () => {
  let component: PriceComparisonPage;
  let fixture: ComponentFixture<PriceComparisonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceComparisonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceComparisonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
