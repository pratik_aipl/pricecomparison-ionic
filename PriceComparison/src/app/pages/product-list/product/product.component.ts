import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, IonSlides } from '@ionic/angular';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  amt:any;
  resultData:any;
  category:any=[];
  constructor(public navParams: NavParams,public tools: Tools,
    public commonServices: CommonService,public modalCtrl: ModalController) {
      this.amt = this.navParams.get('value');
    //  console.log(this.commonServices.getCatgory());
    this.getMasterData()
  }
  ngOnInit() { }


  getMasterData(){
 //   this.tools.openLoader();
     this.commonServices.masterData().then(result => {
     //  this.tools.closeLoader();
       this.resultData=JSON.parse(result.data);
       for (let i = 0; i < this.resultData.data.Category.length; i++) {
         const element = this.resultData.data.Category[i];
         element.IsSelected = true;
         this.category.push(element)         
       }
       console.log(this.category);
      },(error) => {
       this.tools.closeLoader();
       console.log(error);
       this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
     });
   }
   
 
  dismissModal() {
    // environment.isBeaconModalOpen = false;
 //  this.modalCtrl.dismiss('');
  }
  changeChacked(event, i) {
    this.category[i].IsSelected = event.detail.checked; //result.data.userstore;
  }
  close() {

    
    const catId:any = [];
    for (let i = 0; i < this.category.length; i++) {
      if (this.category[i].IsSelected) {
        catId.push(this.category[i].CategoryID);
      }
    }
    if (catId.length == 0) {
      this.tools.openAlert('','Select Category', 'Please select at least one category.');
    } else {
      this.modalCtrl.dismiss(catId);
    }
  }
}
