import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';
import { ModalController } from '@ionic/angular';
import { StoreListComponent } from '../store-list/store-list.component';
import { ProductComponent } from './product/product.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
})
export class ProductListPage implements OnInit {
  inputValue: string = "";
  cat: any;
  catid: any;
  headerStore:any=[];
  headerStore1:any=[];
  LowestStore: any = [];
  productList: any = [];
  productListAll: any = [];
  productListCopy: any = [];
  resultData:any;
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder, public modalController: ModalController,
    public commonService: CommonService,
    public tools: Tools) {
    this.cat = this.activatedRoute.snapshot.paramMap.get('catName');
    this.catid = this.activatedRoute.snapshot.paramMap.get('catid');
    
  }
  backPage() {
    // this.router.navigateByUrl('/');
    this.router.navigateByUrl('/category-list');
  }
  ngOnInit() {
    this.getMyStores();
  }
  inc(item,action,index){
    console.log('Action => ',action);
    console.log('Selected Product => ',item);
    console.log('Index => ',index);
    if(item.Quantity != 1){
      if(action == '-'){
        this.productList[index].Quantity= item.Quantity-1;
      }  
    }
    if(action == '+'){
      this.productList[index].Quantity= item.Quantity+1;
    }
  }

  async productDetails(item) {
    this.commonService.setProduct(item);
    this.router.navigateByUrl('/products-details/'+item.Productid+'/0'+'/'+this.catid);
  }

  myList(){
    this.router.navigateByUrl('/my-list/2/'+this.cat+'/'+this.catid);
  }

  getProductList() {
  //  this.tools.openLoader();
    this.commonService.prodctList().then(result => {
      this.tools.closeLoader();
      console.log('Product List ==> ',JSON.parse(result.data));
      this.productList=JSON.parse(result.data).data.Product;
      for (let i = 0; i < this.productList.length; i++) {
        this.productList[i].Quantity=1;
        this.productList[i].isSelected=true;
      }
      this.productListCopy=this.productList;


      this.productList=[];
      for (let j = 0; j < this.productListCopy.length; j++) {
        const pProduct = this.productListCopy[j];
          if (this.catid == 0 || this.catid == pProduct.categoryid) {
            this.productList.push(pProduct);
          }
      }
      this.productListAll=this.productList;
    }, (error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status, '',JSON.parse(error.error).Message);
    });
  }

  setFilteredItems(evt){
    const searchTerm = evt.srcElement.value;
    if (searchTerm && searchTerm.trim() != '') {
      this.productList=[];
      this.productList=this.productListAll;
      this.productList = this.productList.filter((item) => {
        return (item.Productname.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) > -1);
      })
    }else{
      this.productList=this.productListAll;
    }
  }

  changeChacked(event, i) {
    this.productList[i].isSelected = event.detail.checked ? true : false; //result.data.userstore;
  }
  addProduct(){

    // const prodIds:any=[];
    // for (let i = 0; i < this.productList.length; i++) {
    //   if (this.productList[i].isSelected) {
    //     prodIds.push(this.productList[i].Productid);
    //   }
    // }
    // if(prodIds.length != 0){
    //   this.tools.openLoader();
    //   this.commonService.addProductToStore(prodIds).then(result => {
    //     this.tools.closeLoader();
    //     this.tools.openAlert('', '', JSON.parse(result.data).DMessage);
    //         }, (error) => {
    //     console.log(error);
    //     this.tools.openAlert(error.status, '',JSON.parse(error.error).Message);
    //   });
    // }else{
      
    //   this.tools.openAlert('','Select Product', 'Please Select At Least One Product.');
    // }
  }

  async brandList(){
    const modal = await this.modalController.create({
      component: StoreListComponent,
      cssClass: 'modal',
      componentProps: { value: '' }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log(data);
        this.getMyStores();
        if (data.data) {
          //this.answer.answers[this.currquekey].file = data.data;
        }
      });
  }

  async prodList(){
    const modal = await this.modalController.create({
      component: ProductComponent,
      cssClass: 'modal',
      componentProps: { value: '' }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        if (data.data) {
          console.log('==> select category ',data.data);
          this.productList=[];
          for (let j = 0; j < this.productListCopy.length; j++) {
            const pProduct = this.productListCopy[j];
            for (let k = 0; k < data.data.length; k++) {
              const pcId = data.data[k];
              if (pcId == pProduct.categoryid) {
                this.productList.push(pProduct);
              }
            }
          }
        }
      });
  }

  getMyStores() {
    this.tools.openLoader();
    this.commonService.myStore().then(result => {
      this.resultData=JSON.parse(result.data);
      this.headerStore = this.resultData.data.userstore;
      this.getProductList();
    }, (error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status, '',JSON.parse(error.error).Message);
    });
  }
}
