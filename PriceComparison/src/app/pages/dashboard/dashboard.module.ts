import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardPage } from './dashboard.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductListPage } from '../product-list/product-list.page';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  },
  {
    path: 'product-list/:catName/:catid',
    component: ProductListPage
  }
];

@NgModule({
  imports: [
   SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
  ],
  declarations: [DashboardPage,ProductListPage]
})
export class DashboardPageModule {}
