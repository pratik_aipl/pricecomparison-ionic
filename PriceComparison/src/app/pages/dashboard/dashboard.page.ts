import { Component, OnInit } from '@angular/core';
import { IonSlides, MenuController, ActionSheetController, ModalController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';
import { StoreListComponent } from '../store-list/store-list.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  Category:any=[];
  Subcategory:any=[];
  Store:any=[];
  Banner:any;
  isSub:any=false;
  user:any;
  slideOptions = {
    initialSlide: 1,
    speed: 400,
  };

  constructor(public router: Router,private menu: MenuController,public modalController: ModalController,
    public commonService: CommonService,public actionSheetController: ActionSheetController,
    public tools: Tools,public platform: Platform) { 
      this.user=this.commonService.getUserData();   
  }
  openFirst() {
    this.menu.enable(true, 'first');  // replace with MenuA for your case
    this.menu.open('first');
  }
  ngOnInit() {
  
  }

  ionViewWillEnter(){
    this.cartQty();
  }

  cartQty(){
    this.tools.openLoader();
    this.commonService.cartQty().then(result => {
       console.log( JSON.parse(result.data));       
      if (JSON.parse(result.data).Success) {
        this.tools.closeLoader();
        this.commonService.setQty(JSON.parse(result.data).data.Count);
       // this.getMasterData();
       if ( !this.user.IsUserStore ) {
        this.user.IsUserStore=true;
        this.commonService.setUserData(this.user);
        this.brandList('head');
      }
      }
    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }
  getMasterData(){
   
    this.commonService.masterData().then(result => {
       this.tools.closeLoader();
       console.log( JSON.parse(result.data));
      this.Banner = JSON.parse(result.data).data.Banner;
      this.Store = JSON.parse(result.data).data.Store;
      this.Category = JSON.parse(result.data).data.Category;

      console.log('Check is first ',(localStorage.getItem('isFirst') == undefined || localStorage.getItem('isFirst') !='1' ));
      if (localStorage.getItem('isFirst') == undefined || localStorage.getItem('isFirst') !='1' ) {
        localStorage.setItem('isFirst','1');
        this.brandList('head');
      }
      // this.commonService.setCatgory( this.resultData.data.Category);

    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }
  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  }
  goToCateroy(){
    this.router.navigateByUrl('/category-list');
  }
  clickAll(item){
    this.router.navigateByUrl('/dashboard/product-list/'+item+'/0');
  }
  back(){
    this.isSub=false;
  }
  storeClick(st){
    this.router.navigateByUrl('/store-details/'+st.StoreName+'/'+st.StoreID);
  }
  changePassword(){
    this.menu.close();
    this.router.navigateByUrl('/change-password');
  }
  async brandList(type){
    const modal = await this.modalController.create({
      component: StoreListComponent,
      cssClass: 'modal',
      componentProps: { value: type }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log(data);
        if (data.data) {
          //this.answer.answers[this.currquekey].file = data.data;
        }
      });
  }

  myList(){
    this.router.navigateByUrl('/my-list/0/0/0');
  }
  category(){
    this.menu.close();
    this.goToCateroy();
  }
  storeList(){
    this.menu.close();
    this.brandList('head');
  }
  myPreferredStore(){
    this.menu.close();
    this.brandList('menu');
//    this.router.navigateByUrl('/login');

  }
  logout(){
    this.menu.close();
    localStorage.clear();
    this.tools.presentLogout('Are you sure you want to logout?','Logout','Cancel');
  }

}
