import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CategoryListPage } from './category-list.page';

const routes: Routes = [
  {
    path: '',
    component: CategoryListPage
  },
];

@NgModule({
  imports: [
   SharedModule,
   RouterModule.forChild(routes),
   FormsModule,
  ],
  declarations: [CategoryListPage]
})
export class CategoryListPageModule {}
