import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/shared/tools';
import { ModalController } from '@ionic/angular';
import { StoreListComponent } from '../store-list/store-list.component';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.page.html',
  styleUrls: ['./category-list.page.scss'],
})
export class CategoryListPage implements OnInit {
  ngOnInit(): void {
    //this.getMasterData();
  }
  inputValue: string = "";
  
 Category:any=[];
  Subcategory:any=[];
  SubSubcategory:any=[];
 CategoryAll:any=[];
  SubcategoryAll:any=[];
  SubSubcategoryAll:any=[];
  Store:any=[];
  Banner:any;
  isSub:any=false;
  isCatSub:any=false;
  user:any;
  resultData:any;
  resultDataSub:any;
  title:any;
  catTitle:any;
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder, public modalController: ModalController,
    public commonService: CommonService,
    public tools: Tools) {
      this.title='Category';
         this.user=this.commonService.getUserData();
        
  }
  ionViewWillEnter(){
    this.cartQty();
  }

  cartQty(){
    this.tools.openLoader();
    this.commonService.cartQty().then(result => {
       console.log( JSON.parse(result.data));       
      if (JSON.parse(result.data).Success) {
        this.commonService.setQty(JSON.parse(result.data).data.Count);
        this.getMasterData();
      }
    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }
  getMasterData(){
   
    this.commonService.masterData().then(result => {
       this.tools.closeLoader();
       console.log( JSON.parse(result.data));
      this.Banner = JSON.parse(result.data).data.Banner;
      this.Store = JSON.parse(result.data).data.Store;
      this.Category = JSON.parse(result.data).data.Category;
      this.CategoryAll = JSON.parse(result.data).data.Category;

      console.log('Check is first ',(localStorage.getItem('isFirst') == undefined || localStorage.getItem('isFirst') !='1' ));
      if ( !this.user.IsActive ) {
        this.brandList();
      }
      // this.commonService.setCatgory( this.resultData.data.Category);

    },(error) => {
      this.tools.closeLoader();
      console.log(error);
      this.tools.openAlert(error.status,'',JSON.parse(error.error).Message);
    });
  }
  backPage() {
    this.router.navigateByUrl('/');
  }

  myList(){
    this.router.navigateByUrl('/my-list/1/0/0');
  }

  getSubCategory(catId){
    this.tools.openLoader();
     this.commonService.subCategory(catId).then(result => {
       this.tools.closeLoader();
       if(!this.isCatSub){
         this.isSub=true;      
         this.Subcategory = JSON.parse(result.data).data.Subcategory;
         this.SubcategoryAll = JSON.parse(result.data).data.Subcategory;
       }else{
        this.SubSubcategory = JSON.parse(result.data).data.Subcategory;        
        this.SubSubcategoryAll = JSON.parse(result.data).data.Subcategory;        
       }
     },(error) => {
       this.tools.closeLoader();
       console.log(error);
       this.tools.openAlert(error.status,error.statusText,error.message);
     });
   }

   setFilteredItems(evt){

    const searchTerm = evt.srcElement.value;

    // if (!searchTerm) {
    //   return;
    // }
     console.log('Filter Value ',searchTerm)
     if (!this.isSub) {
      if (searchTerm && searchTerm.trim() != '') {
        this.Category=[];
        this.Category=this.CategoryAll;
        this.Category = this.Category.filter((item) => {
          return (item.CategoryName.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) > -1);
        })
      }else{
        this.Category=this.CategoryAll;
      }
     }
     if (this.isSub && !this.isCatSub) {
      if (searchTerm && searchTerm.trim() != '') {
        // this.isItemAvailable = true;
        this.Subcategory = this.Subcategory.filter((item) => {
          return (item.CategoryName.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) > -1);
        })
      }else{
        this.Subcategory=this.Subcategory;
      }
     }
     if (this.isCatSub) {
      if (searchTerm && searchTerm.trim() != '') {
        // this.isItemAvailable = true;
        this.SubSubcategory = this.SubSubcategory.filter((item) => {
          return (item.CategoryName.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) > -1);
        })
      }else{
        this.SubSubcategoryAll=this.SubSubcategoryAll;
      }
     }

  }
  categoryClick(cat){
    if(cat.IsSubcat != undefined && cat.IsSubcat=='1'){
      this.title=cat.CategoryName;
      this.catTitle=cat.CategoryName;
      this.getSubCategory(cat.CategoryID);
    }else{
      if (cat.TotalProduct === 0) {
          this.tools.openAlert('','','Product not awailable for this category.');
      }else{
        cat.CategoryName=cat.CategoryName.replace('/','-');
         this.router.navigateByUrl('/dashboard/product-list/'+cat.CategoryName+'/'+cat.CategoryID);
      }
    }
  }
  categorySubClick(cat){
    if(cat.IsSubcat != undefined && cat.IsSubcat=='1'){
      this.isCatSub=true;
      this.isSub=false;
      this.title=cat.CategoryName;
      this.getSubCategory(cat.CategoryID);
    }else{
      if (cat.TotalProduct === 0) {
          this.tools.openAlert('','','Product not awailable for this category.');
      }else{
        cat.CategoryName=cat.CategoryName.replace('/','-');
         this.router.navigateByUrl('/dashboard/product-list/'+cat.CategoryName+'/'+cat.CategoryID);
      }
    }
  }
  clickAll(item){
     this.router.navigateByUrl('/dashboard/product-list/'+item+'/0');
  }
  back(){
    this.title='Category';
    this.isSub=false;
    this.isCatSub=false;
  }
  backSub(){
    this.title= this.catTitle;
    this.isSub=true;
    this.isCatSub=false;
  }
  storeClick(st){
    this.router.navigateByUrl('/store-details/'+st.StoreName+'/'+st.StoreID);
  }
  async brandList(){
    const modal = await this.modalController.create({
      component: StoreListComponent,
      cssClass: 'modal',
      componentProps: { value: '' }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log(data);
        if (data.data) {
          //this.answer.answers[this.currquekey].file = data.data;
        }
      });
  }

}
