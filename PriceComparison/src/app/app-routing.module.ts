import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/authguard.service';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'forgot-password', loadChildren: './pages/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'change-password',canActivate: [AuthGuard], loadChildren: './pages/change-password/change-password.module#ChangePasswordPageModule' },
  { path: 'dashboard',  canActivate: [AuthGuard],loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule' },
  { path: 'store-details/:store/:stid', canActivate: [AuthGuard], loadChildren: './pages/store-details/store-details.module#StoreDetailsPageModule' },
  { path: 'my-list/:from/:catName/:catid', canActivate: [AuthGuard], loadChildren: './pages/my-list/my-list.module#MyListPageModule' },
  { path: 'products-details/:prodId/:type/:catType', canActivate: [AuthGuard], loadChildren: './pages/product-list/products-details/products-details.module#ProductsDetailsPageModule' },
  { path: 'category-list',  canActivate: [AuthGuard],loadChildren: './pages/category-list/category.module#CategoryListPageModule' },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
