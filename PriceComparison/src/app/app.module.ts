import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './shared/authguard.service';
import { Tools } from './shared/tools';
import {  ReactiveFormsModule } from '@angular/forms';
import { StoreListComponent } from './pages/store-list/store-list.component';
import { ProductComponent } from './pages/product-list/product/product.component';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { HTTP } from '@ionic-native/http/ngx';
@NgModule({
  declarations: [AppComponent,StoreListComponent,ProductComponent],
  entryComponents: [StoreListComponent,ProductComponent],
  imports: [BrowserModule,
    ReactiveFormsModule, IonicModule.forRoot({
      mode: 'md',
      scrollAssist: false
    }), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    AuthGuard,
    HTTP,
    CallNumber,
    Tools,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: HttpConfigInterceptor,
    //   multi: true
    // }
  ],
  bootstrap: [AppComponent]
})
/*
{
      mode: 'md',
      scrollAssist: false
    }
    */
export class AppModule {}
