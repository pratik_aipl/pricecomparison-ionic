import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';
import { HTTP } from '@ionic-native/http/ngx';
import { Tools } from './tools';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  deviceInfo;
  bacisAuth;
  options;
  constructor(public auth: AuthGuard, public tool: Tools, private http: HTTP,   public router: Router,) {
   
    
  }

  setHeaderData() {
    if (this.auth.canActivate && this.getLoginToken() && this.getUserId()) {
      console.log('Login Token --> ',this.getLoginToken());
      this.options = {'Authorization': this.getLoginToken()}
    }
  }

  // private handleError<T> (operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {
  //     // TODO: send the error to remote logging infrastructure
  //     console.error(error); // log to console instead
  //     if(status =='500'){
  //       this.tool.openAlert(error.status,error.error.UMessage,error.error.DMessage);    
  //     }else{
  //       this.tool.openAlert(error.status,error.statusText,error.message);    
  //     }
  //     return of(result as T);
  //   };
  // }


  toekn(data): any {
   let  params={ username:data.emailid,password:data.pwd,grant_type:'password'}
    return this.http.post(environment.BaseUrl + 'token', params,{});
  }
  login(data): any {
    return this.http.post(environment.BaseUrl + environment.apiend + 'user/Login',data,{});
  }
  changepassword(data): any {
    this.setHeaderData();
    let  params={UserID:this.getUserId(),OldPassword: data.OldPassword,NewPassword: data.NewPassword}
    return this.http.post(environment.BaseUrl + environment.apiend + 'user/ChangePassword', params,this.options);
  }
  forgotPassword(data): any {
    return this.http.get(environment.BaseUrl + environment.apiend + 'user/ForgotPassword?EmailID='+data.email,{},{});
  }
  register(data): any {
    
    let params = {
      Fname: data.fname,
      LName: data.lname,
      EmailID: data.emailid,
      UPassword: data.pwd,
      MobileNo: data.mphone
    }
    console.log(params);
    return this.http.post(environment.BaseUrl + environment.apiend + 'user', params,{});
  }

  masterData() {
    return this.http.get(environment.BaseUrl + environment.apiend + 'user/MasterData',{},{});
  }
  subCategory(catId) {
    console.log(catId)
    return this.http.get(environment.BaseUrl + environment.apiend + 'category/Subcategory?CatID=' + catId,{},{});
  }

  userStore() {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + environment.apiend + 'store/UserStore?UserID='+this.getUserId(),{}, this.options);
  }

  prodctList() {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + environment.apiend + 'product',{}, this.options);
  }
  myProduct() {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + environment.apiend + 'product/ProductByUserID?UserID='+this.getUserId() ,{} ,this.options);
  }
  getStoreDetailsById(id) {
    console.log(id)
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + environment.apiend + 'store/StoreDetByID?StoreID=' + id,{}, this.options);
  }
  getProductCommentsById(id) {
    console.log(id)
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + environment.apiend + 'product/ProductCommentByID?UserID='+this.getUserId()+'&ProductID=' + id,{}, this.options);
  }
  createComment(id,comment) {
    let params = {
      UserID: this.getUserId(),
      ProductID: id,
      Comment: comment  
     }
    console.log(params);    
    return this.http.post(environment.BaseUrl + environment.apiend + 'product/ProductComment',params, {});
  }
  saveStore(storeId) {
    let params={
    StoreID:storeId.join(","),
    UserID: this.getUserId()
    }

  return this.http.post(environment.BaseUrl + environment.apiend + 'store/SaveStorePreference', params,{});
  }
  addProductToStore(prodId,qt) {
this.setHeaderData();
    const params = {
    ProductID:prodId.join(","),
    Quantity:qt,
    UserID: this.getUserId()
    }
    
  return this.http.post(environment.BaseUrl + environment.apiend + 'product', params,this.options);
  }
  removeProduct(prodId) {
    this.setHeaderData();
  return this.http.delete(environment.BaseUrl + environment.apiend + 'product/DeleteByProduct?ProductID='+prodId+'&UserID='+this.getUserId(),{}, this.options);
  }
  myStore() {
    this.setHeaderData();
  return this.http.get(environment.BaseUrl + environment.apiend + 'store/MyPreferenceStore?UserID='+this.getUserId(),{}, this.options);
  }
  cartQty() {
    this.setHeaderData();
  return this.http.get(environment.BaseUrl + environment.apiend + 'product/ProductCount?UserID='+this.getUserId(),{}, this.options);
  }

  getCurrentLatLng() {
    navigator.geolocation.getCurrentPosition((res) => {
      let currentLatLng: any = {};
      currentLatLng.latitude = res.coords.latitude;
      currentLatLng.longitude = res.coords.longitude;
      return currentLatLng;
    });
  }

  // GET & SET USER DATA
  setCatgory(catData) {
    console.log('save category ==> ', catData);
    window.localStorage.setItem('cat_data', JSON.stringify(catData));
  }
  getCatgory(){
    if (window.localStorage['cat_data']) {
      return JSON.parse(window.localStorage['cat_data']);
    }
  }

   // GET & SET USER DATA
   setProduct(prodData) {
    console.log('save category ==> ', prodData);
    window.localStorage.setItem('prodData', JSON.stringify(prodData));
  }
  getProduct(){
    if (window.localStorage['prodData']) {
      return JSON.parse(window.localStorage['prodData']);
    }
  }
  
  setQty(qty) {
    console.log('save category ==> ', qty);
    window.localStorage.setItem('qty',qty);
  }
  getQty(){
    if (window.localStorage['qty']) {
      return window.localStorage['qty'];
    }else{
      return 0;
    }
  }

  setUserData(userData) {
    console.log(userData);
    window.localStorage.setItem('user_data', JSON.stringify(userData));
    window.localStorage.setItem('user_id', userData.UserID);
  }
  setToken(login_token) {
    if (login_token != '')
      window.localStorage.setItem('login_token', login_token);
  }
  getLoginToken() {
    if (window.localStorage['login_token']) {
      return window.localStorage['login_token'];
    }
    return;
  }
  getUserData() {
    if (window.localStorage['user_data']) {
      return JSON.parse(window.localStorage['user_data']);
    }
    return;
  }
  getUserId() {
    if (window.localStorage['user_id']) {
      return window.localStorage['user_id'];
    }
    return;
  }


  getKeyPairValue(param) {
    let formBody: any = [];
    // tslint:disable-next-line:forin
    for (let property in param) {
      formBody.push(encodeURIComponent(property) + '=' + encodeURIComponent(param[property]));
    }
    formBody = formBody.join('&');
    return formBody;
  }
}
